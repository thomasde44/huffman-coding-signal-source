# Huffman Coding in python
import math
import random
import os

signal_samples = []

# 2 creating the 10000 samples
for i in range(10000):
    t = random.uniform(-5, 5)
    signal_samples.append(3*math.cos(2*math.pi*6000*(t)) + 1*math.cos(2*math.pi*3000*(t)))

frequency_count = {
    "0000": 0, "0001": 0, "0010": 0, "0011": 0, 
    "0100": 0, "0101": 0, "0110": 0, "0111": 0, 
    "1000": 0, "1001": 0, "1010": 0, "1011": 0, 
    "1100": 0, "1101": 0, "1110": 0, "1111": 0
}

probability = {
    "0000": 0, "0001": 0, "0010": 0, "0011": 0, 
    "0100": 0, "0101": 0, "0110": 0, "0111": 0, 
    "1000": 0, "1001": 0, "1010": 0, "1011": 0, 
    "1100": 0, "1101": 0, "1110": 0, "1111": 0
}

f = open("samples_no_coding.txt", "w")
f2 = open("samples_coding.txt", "w")

no_coding = []
# 40.0 kB (40,000 bytes) size of samples.txt after this step

# 5 save the encoded samples in files and count symbol appearances
for i in signal_samples:

    if i >= -4.0 and i < -3.5:
        frequency_count["0000"] += 1
        no_coding.append("0000")
        f.write("0000")
    if i >= -3.5 and i < -3.0:
        frequency_count["0001"] += 1
        no_coding.append("0001")
        f.write("0001")
    if i >= -3.0 and i < -2.5:
        frequency_count["0010"] += 1
        no_coding.append("0010")
        f.write("0010")
    if i >= -2.5 and i < -2.0:
        frequency_count["0011"] += 1
        no_coding.append("0011")
        f.write("0011")
    if i >= -2.0 and i < -1.5:
        frequency_count["0100"] += 1
        no_coding.append("0100")
        f.write("0100")
    if i >= -1.5 and i < -1.0:
        frequency_count["0101"] += 1
        no_coding.append("0101")
        f.write("0101")
    if i >= -1.0 and i < -0.5:
        frequency_count["0110"] += 1
        no_coding.append("0110")
        f.write("0110")
    if i >= -0.5 and i < 0.0:
        frequency_count["0111"] += 1
        no_coding.append("0111")
        f.write("0111")
    if i >= 0.0 and i < 0.5:
        frequency_count["1000"] += 1
        no_coding.append("1000")
        f.write("1000")
    if i >= 0.5 and i < 1.0:
        frequency_count["1001"] += 1
        no_coding.append("1001")
        f.write("1001")
    if i >= 1.0 and i < 1.5:
        frequency_count["1010"] += 1
        no_coding.append("1010")
        f.write("1010")
    if i >= 1.5 and i < 2.0:
        frequency_count["1011"] += 1
        no_coding.append("1011")
        f.write("1011")
    if i >= 2.0 and i < 2.5:
        frequency_count["1100"] += 1
        no_coding.append("1100")
        f.write("1100")
    if i >= 2.5 and i < 3.0:
        frequency_count["1101"] += 1
        no_coding.append("1101")
        f.write("1101")
    if i >= 3.0 and i < 3.5:
        frequency_count["1110"] += 1
        no_coding.append("1110")
        f.write("1110")
    if i >= 3.5 and i < 4.0:
        frequency_count["1111"] += 1
        no_coding.append("1111")
        f.write("1111")

print(frequency_count)
print(" ")

# 3 turning the frequency count into probabilities
for i in frequency_count:
    probability[i] = frequency_count[i]/10000

sorted_probability = dict(sorted(probability.items(), key=lambda item: item[1]))
# display the probabilities
print(' Symbol | Probability ')
print('----------------------')
for i in sorted_probability:
    print(' %-4r |%12s' % (i, sorted_probability[i]))


# node class to make tree
class NodeTree(object):

    def __init__(self, left=None, right=None):
        self.left = left
        self.right = right

    def children(self):
        return (self.left, self.right)

    def nodes(self):
        return (self.left, self.right)

    def __str__(self):
        return '%s_%s' % (self.left, self.right)


# take the nodes and make a huffman tree
def huffman_code_tree(node, left=True, binString=''):
    if type(node) is str:
        return {node: binString}
    (l, r) = node.children()
    d = dict()
    d.update(huffman_code_tree(l, True, binString + '0'))
    d.update(huffman_code_tree(r, False, binString + '1'))
    return d


# Calculating frequency
freq = {}
for c in no_coding:
    if c in freq:
        freq[c] += 1
    else:
        freq[c] = 1

# 6 and 7 use the node class to get the new encoding
freq = sorted(freq.items(), key=lambda x: x[1], reverse=True)

nodes = freq
while len(nodes) > 1:
    (key1, c1) = nodes[-1]
    (key2, c2) = nodes[-2]
    nodes = nodes[:-2]
    node = NodeTree(key1, key2)
    nodes.append((node, c1 + c2))

    nodes = sorted(nodes, key=lambda x: x[1], reverse=True)
print()
huffmanCode = huffman_code_tree(nodes[0][0])

print(' Symbol | Huffman code ')
print('----------------------')
for (char, frequency) in freq:
    print(' %-4r |%12s' % (char, huffmanCode[char]))

# 8 saving the encoded stream into a second file
for i in no_coding:
    f2.write(huffmanCode[i])
print(" ")

# 4 calculate the entropy
entropy = 0
for i in sorted_probability:
    if sorted_probability[i] != 0:
        entropy += sorted_probability[i] * math.log(sorted_probability[i], 2)

avg_code_length = 0

# calculate the r value for efficiency
for i in huffmanCode:
    avg_code_length += len(huffmanCode[i]) * sorted_probability[i]



print("r: ", avg_code_length)
entropy = -entropy
print("entropy of the encoded signal: ", entropy)

print("theoretical efficiency: " , entropy/avg_code_length)

# file1 = os.path.getsize("./samples_no_coding.txt")
# file2 = os.path.getsize("./samples_coding.txt")

# print("actual efficiency: ", file2/file1)
